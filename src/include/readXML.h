#ifndef READ_XML
#define READ_XML

#include <pugixml.hpp>

namespace readXML
{
  template<typename T> void readValueXML(const std::string &file, T &variable, const std::string &name ,const T &defaultValue);

  void readValueXML(const std::string &file, double &variable, const std::string &name ,const double &defaultValue);

}

#endif
