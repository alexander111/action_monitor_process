/*!*****************************************************************************
 *  \file       action_monitor_process.h
 *  \brief      ActionMonitor definition file.
 *  \details    This file contains the ActionMonitor declaration.
 *  \author     David Palacios
 *  \copyright  Copyright 2016 Universidad Politecnica de Madrid (UPM) 
 *
 *     This program is free software: you can redistribute it and/or modify 
 *     it under the terms of the GNU General Public License as published by 
 *     the Free Software Foundation, either version 3 of the License, or 
 *     (at your option) any later version. 
 *   
 *     This program is distributed in the hope that it will be useful, 
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 *     GNU General Public License for more details. 
 *   
 *     You should have received a copy of the GNU General Public License 
 *     along with this program. If not, see http://www.gnu.org/licenses/. 
 ********************************************************************************/

#ifndef ACTION_MONITOR_H
#define ACTION_MONITOR_H

//ROS
#include <ros/ros.h>
#include <angles/angles.h>

//DroneProcess
#include <drone_process.h>

//Messages
#include <droneMsgsROS/dronePose.h>
#include <droneMsgsROS/droneSpeeds.h>
#include <droneMsgsROS/actionData.h>
#include <droneMsgsROS/CompletedAction.h>
#include <droneMsgsROS/actionArguments.h>
#include <geometry_msgs/Point.h>
#include <droneMsgsROS/droneStatus.h>


//C lib
#include <string>
#include <iostream>
#include <cmath>

#include "readXML.h"
#include "timer.h"

/*!********************************************************************************************************************
 *  \class      ActionMonitor
 *  \brief      The ActionMonitor is the node responsible of monitoring the actions.
 *  \details    The ActionMonitor receives an approved action from de Behavior Manager and checks when it finishes, 
 *              if it is interrupted and if the action lasts too much. The ActionMonitor sends the action ending value
 *              to the Mission Planner.
 *********************************************************************************************************************/
class ActionMonitor : public DroneProcess
{
// Variables
private:
  //Timer
  Timer timer;
  
  //string that stores the name of the action
  std::string action_name;

  //precision values
  //Store the different values read from the XML file
  double precision_take_off, precision_land, precision_null_axis_speed, precision_null_rotation_speed;
  double precision_null_pose, precision_null_attitude, precision_null_rad_yaw;

  //booleans
  bool action_in_process;
  bool current_speed_initialized;

  bool waiting;//Indicates if an action is waiting for timer confirmation

  //Should point to precision.xml
  std::string precision_file;                             //!< Attribute storing the path to the XML document which contains the precision values
  std::string precision_file_dir;
  std::string my_stack_directory;
  std::string drone_id_namespace;
  std::string drone_id;

  //Topic name strings
  std::string drone_pose;
  std::string drone_speed;
  std::string approved_action;
  std::string completed_action;

  //Handle
  ros::NodeHandle n;

  //Subscriber
  ros::Subscriber drone_pose_sub;                        //!< ROS subscriber handler of topic drone_pose
  ros::Subscriber drone_speed_sub;                       //!< ROS subscriber handler to topic drone_speed
  ros::Subscriber approved_action_sub;                   //!< ROS subscriber handler to topic approved_action; receives the approved action from the Behaviour Manager

  //Publisher
  ros::Publisher completed_action_pub;                   //!< ROS publisher handler to topic completed_action; used to send completed action to the Mission Planner

  //Last state variables
  //Store the last state if pertinent (sometimes new states are ignored)
  droneMsgsROS::dronePose current_pose;                  //Attribute storing the current pose. Only modified when the value action_in_process is true
  droneMsgsROS::droneSpeeds current_speed;               //Attribute storing the current speed. Only modified when the value action_in_process is true
  droneMsgsROS::actionData current_action;               //Attribute storing the current monitored action.
  
  geometry_msgs::Point target_position;                  //Attribute storing the theorical final pose. Only used with actions GO_TO_POINT and MOVE.

// Methods	
public:
  //! Constructor. \details It needs the same arguments as the ros::init function.
  ActionMonitor();
  ~ActionMonitor();

protected:
  /*!******************************************************************************************************************
   * \brief DroneProcess inherited functions
   *******************************************************************************************************************/ 
  void ownSetUp();
  void ownStart();
  void ownStop();
  void ownRun();


private:
  //callbacks

  /*!***************************************************************************************
   * \details This is the method called every time a new message arrives to the drone_pose_sub
   * \param msg The received message that needs to be processed.
   * \return Void function
   ****************************************************************************************/
  void dronePoseCallback(const droneMsgsROS::dronePose &msg);

  /*!***************************************************************************************
   * \details This is the method called every time a new message arrives to the drone_speed_sub
   * \param msg The received message that needs to be processed.
   * \return Void function
   ****************************************************************************************/
  void droneSpeedCallback(const droneMsgsROS::droneSpeeds &msg);

  /*!***************************************************************************************
   * \details This is the method called every time a new message arrives to approved_action_sub.
              It activates the appropiate timeout.
   * \param msg The received message that needs to be processed.
   * \return Void function
   ****************************************************************************************/
  void approvedActionCallback(const droneMsgsROS::actionData &msg);

private:
  //Other functions

  /*!***************************************************************************************
   * \details This method is called every time a new Pose or Speed msg arrives, but only when
   *          an action is being monitored. It checks wheter the action is completed or not.
   * \return Void function
   ****************************************************************************************/
  void check();

  /*!***************************************************************************************
   * \details Sends a msg when the action finishes, is interrupted or the timeout is signaled.
   * \param ack the value of the msg, true if the action finished succesfully, false otherwise.
   * \return Void function
   ****************************************************************************************/
  void sendConfirmation(bool successful, bool timeout=false, bool interrupted=false);

};

#endif
