#ifndef TIMER_ACTION_MONITOR
#define TIMER_ACTION_MONITOR

#include <boost/thread.hpp>
#include <boost/chrono.hpp>
//#include <boost/atomic.hpp>

#include <iostream>


class Timer 
{
private:
  boost::thread  m_Thread;
  boost::condition_variable cond;
  boost::mutex mut;

  int secs;
  bool res;
  bool sleeping;
  bool terminar;

public:
  //! Constructor.
  Timer();
  ~Timer();

  void time(int seconds);
  
  void stop();

  void join();

  bool getResult();

private:
  void threadFunc();

};

#endif
