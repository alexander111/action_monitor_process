#include "../include/readXML.h"

void readXML::readValueXML(const std::string &file, double &variable, const std::string &name, const double &defaultValue)
{
  bool res = true; //false if variable = defaultValue
  pugi::xml_document doc;
  pugi::xml_parse_result result = doc.load_file(file.c_str());
  pugi::xml_node selectedNode;
  pugi::xml_attribute selectedAttribute;
  if ( res = res && result )
  {
    selectedNode = doc.child(name.c_str());
  }
  if ( res = res && !selectedNode.empty() )
  {
    selectedAttribute = selectedNode.attribute("value");
  }
  if ( res = res && !selectedAttribute.empty() )
  {
    variable = selectedAttribute.as_double();

  }
  if ( !res )
  {
    variable = defaultValue;
  }
}