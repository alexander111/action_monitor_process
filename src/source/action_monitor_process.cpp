#include "../include/action_monitor_process.h"

using namespace std;
ActionMonitor::ActionMonitor() : DroneProcess()
{
  //set variables
  action_in_process = false;
  current_speed_initialized = false;
  action_name="Error";
  waiting=false;
}

ActionMonitor::~ActionMonitor()
{
  
}

void ActionMonitor::ownSetUp()
{
  //Looks for argument and stores it, if it is not defined a default value is stored 
  n.param<string>("drone_pose", drone_pose, "EstimatedPose_droneGMR_wrt_GFF");
  n.param<string>("drone_speed", drone_speed, "EstimatedSpeed_droneGMR_wrt_GFF");
  n.param<string>("approved_action", approved_action, "approved_action");
  n.param<string>("completed_action", completed_action, "completed_action");

  n.param<string>("drone_id", drone_id, "1");
  n.param<string>("drone_id_namespace", drone_id_namespace, "drone"+drone_id);
  n.param<string>("my_stack_directory", my_stack_directory, "~/workspace/ros/quadrotor_stack_catkin/src/quadrotor_stack");
  n.param<string>("precision_file", precision_file, "action_precision.xml");
  n.param<string>("precision_file_dir", precision_file_dir, "");

  if (precision_file_dir.empty())
  {
    precision_file_dir=my_stack_directory+"/configs/"+drone_id_namespace;
  }

  cout << "Reading from file: " << precision_file_dir << "/" << precision_file << endl;
  //Read precision values from XML file
  readXML::readValueXML(precision_file_dir+"/"+precision_file, precision_take_off, "precision_take_off",0.6);
  readXML::readValueXML(precision_file_dir+"/"+precision_file, precision_land, "precision_land",0.2);
  readXML::readValueXML(precision_file_dir+"/"+precision_file, precision_null_axis_speed, "precision_null_axis_speed",0.05);
  readXML::readValueXML(precision_file_dir+"/"+precision_file, precision_null_rotation_speed, "precision_null_rotation_speed",0.1);//Only pertinent when checking speed in ROTATE_YAW
  readXML::readValueXML(precision_file_dir+"/"+precision_file, precision_null_pose, "precision_null_pose",0.1);
  readXML::readValueXML(precision_file_dir+"/"+precision_file, precision_null_attitude, "precision_null_attitude",0.5);
  readXML::readValueXML(precision_file_dir+"/"+precision_file, precision_null_rad_yaw, "precision_null_rad_yaw",0.25);//0.25 radians, aprox 11.459 degrees

  cout << "Take off precision: "             << precision_take_off << endl;
  cout << "Land precision: "                 << precision_land << endl;
  cout << "Null axis speed precision: "      << precision_null_axis_speed << endl;
  cout << "Null rotation speed precision: "  << precision_null_rotation_speed << endl;
  cout << "Null pose precision: "            << precision_null_pose << endl;
  cout << "Yaw precision: "                  << precision_null_rad_yaw << endl;

}

void ActionMonitor::ownStart()
{
  // set subscribers
  drone_pose_sub = n.subscribe(drone_pose, 1000, &ActionMonitor::dronePoseCallback, this);
  drone_speed_sub = n.subscribe(drone_speed, 1000, &ActionMonitor::droneSpeedCallback, this);
  approved_action_sub = n.subscribe(approved_action, 1000, &ActionMonitor::approvedActionCallback, this);

  // set publishers
  completed_action_pub = n.advertise<droneMsgsROS::CompletedAction>(completed_action, 10);
}

void ActionMonitor::ownStop()
{
  drone_pose_sub.shutdown();
  drone_speed_sub.shutdown();
  approved_action_sub.shutdown();

  completed_action_pub.shutdown();
}

void ActionMonitor::ownRun()
{
  if (action_in_process && timer.getResult() && !waiting)
  {
    sendConfirmation(false,true);
  }
}

void ActionMonitor::dronePoseCallback(const droneMsgsROS::dronePose &msg)
{
  //this callback is always modifying the current_pose because we need this value to calculate the final pose when current action is moving
  current_pose=msg;
  check();
}

void ActionMonitor::droneSpeedCallback(const droneMsgsROS::droneSpeeds &msg)
{
  //this callback is only necessary when there is an action being monitored
  if (action_in_process)
  {
    current_speed=msg;
    current_speed_initialized = true;
    check();
  }
}

void ActionMonitor::approvedActionCallback(const droneMsgsROS::actionData &msg)
{
  if (msg.ack)
  {
    int32_t last_action_request = msg.mpAction;
    switch(last_action_request)
    {
      case droneMsgsROS::actionData::TAKE_OFF:
        if(action_in_process)
          sendConfirmation(false,false,true);
        action_name = "Take off";
        waiting=false;
        break;
      case droneMsgsROS::actionData::HOVER:
        if(action_in_process)
          sendConfirmation(false,false,true);
        action_name = "Hover";
        timer.time(msg.arguments.at(0).value.at(0));
        waiting=true;
        break;
      case droneMsgsROS::actionData::LAND:
        if(action_in_process)
          sendConfirmation(false,false,true);
        action_name = "Land";
        waiting=false;
        break;
      case droneMsgsROS::actionData::STABILIZE:
        if(action_in_process)
          sendConfirmation(false,false,true);
        action_name = "Stabilize";
        waiting=false;
        break;
      case droneMsgsROS::actionData::MOVE:
        if(action_in_process)
          sendConfirmation(false,false,true);
        action_name = "Move";
        target_position.x=msg.arguments.at(0).value.at(0) + current_pose.x;
        target_position.y=msg.arguments.at(0).value.at(1) + current_pose.y;
        target_position.z=msg.arguments.at(0).value.at(2) + current_pose.z;
        waiting=false;
        break;
      case droneMsgsROS::actionData::GO_TO_POINT:
        if(action_in_process)
          sendConfirmation(false,false,true);
        action_name = "Go to point";
        target_position.x=msg.arguments.at(0).value.at(0);
        target_position.y=msg.arguments.at(0).value.at(1);
        target_position.z=msg.arguments.at(0).value.at(2);
        waiting=false;
        break;
      case droneMsgsROS::actionData::ROTATE_YAW:
        if(action_in_process)
          sendConfirmation(false,false,true);
        action_name = "Rotate yaw";
        waiting=false;
        break;
      case droneMsgsROS::actionData::FLIP:
        if(action_in_process)
          sendConfirmation(false,false,true);
        action_name = "Flip";
        timer.time(3);
        waiting=true;
        break;
      case droneMsgsROS::actionData::FLIP_RIGHT:
        if(action_in_process)
          sendConfirmation(false,false,true);
        action_name = "Flip right";
        timer.time(3);
        waiting=true;
        break;
      case droneMsgsROS::actionData::FLIP_LEFT:
        if(action_in_process)
          sendConfirmation(false,false,true);
        action_name = "Flip left";
        timer.time(3);
        waiting=true;
        break;
      case droneMsgsROS::actionData::FLIP_FRONT:
        if(action_in_process)
          sendConfirmation(false,false,true);
        action_name = "Flip front";
        timer.time(3);
        waiting=true;
        break;
      case droneMsgsROS::actionData::FLIP_BACK:
        if(action_in_process)
          sendConfirmation(false,false,true);
        action_name = "Flip back";
        timer.time(3);
        waiting=true;
        break;
      default:
        ROS_FATAL("Action received not valid. Action id cannot be %d", msg.mpAction);
        return;
    }
    ROS_INFO("Received action %s", action_name.c_str());

    current_action=msg;

    current_speed_initialized=false;  //It is assumed that a new action begins. The previous value is "lost".
    action_in_process=true;
    if (!waiting)
    {
      timer.time(20);//if the action is not completed in 20 seconds, it will send action ack false
    }
  }
}

void ActionMonitor::check()
{
  if ( action_in_process && current_speed_initialized )
  {
    int32_t last_action_request = current_action.mpAction;
    switch(last_action_request)
    {
      case droneMsgsROS::actionData::TAKE_OFF:
        if ( current_pose.z > precision_take_off && abs(current_speed.dz) < precision_null_axis_speed )
        {
          sendConfirmation(true);
        }
        break;
      case droneMsgsROS::actionData::STABILIZE:
        if ( abs(current_pose.pitch) < precision_null_attitude && abs(current_pose.roll) < precision_null_attitude && \
                abs(current_speed.dx) < precision_null_axis_speed && abs(current_speed.dy) < precision_null_axis_speed && \
                abs(current_speed.dz) < precision_null_axis_speed && abs(current_speed.dyaw) < precision_null_axis_speed && \
                abs(current_speed.dpitch) < precision_null_axis_speed && abs(current_speed.droll) < precision_null_axis_speed )
        {
          sendConfirmation(true);
        }
        break;
      case droneMsgsROS::actionData::HOVER:
        if ( abs(current_pose.pitch) < precision_null_attitude && abs(current_pose.roll) < precision_null_attitude && \
                abs(current_speed.dx) < precision_null_axis_speed && abs(current_speed.dy) < precision_null_axis_speed && \
                abs(current_speed.dz) < precision_null_axis_speed && abs(current_speed.dyaw) < precision_null_axis_speed && \
                abs(current_speed.dpitch) < precision_null_axis_speed && abs(current_speed.droll) < precision_null_axis_speed && \
                timer.getResult() )
        {
          sendConfirmation(true);
        }
        break;
      case droneMsgsROS::actionData::LAND:
        if ( current_pose.z < precision_land && abs(current_speed.dz) < precision_null_axis_speed )
        {
          sendConfirmation(true);
        }
        break;
      case droneMsgsROS::actionData::MOVE:
        if ( abs(target_position.x-current_pose.x)<precision_null_pose && abs(target_position.y-current_pose.y)<precision_null_pose && \
                abs(target_position.z-current_pose.z)<precision_null_pose && abs(current_speed.dx) < precision_null_axis_speed &&  \
                abs(current_speed.dy) < precision_null_axis_speed && abs(current_speed.dz) < precision_null_axis_speed )
        {
          sendConfirmation(true);
        }
        break;
      case droneMsgsROS::actionData::GO_TO_POINT:
        if ( abs(target_position.x-current_pose.x)<precision_null_pose && abs(target_position.y-current_pose.y)<precision_null_pose && \
                abs(target_position.z-current_pose.z)<precision_null_pose && abs(current_speed.dx) < precision_null_axis_speed &&  \
                abs(current_speed.dy) < precision_null_axis_speed && abs(current_speed.dz) < precision_null_axis_speed )
        {
          sendConfirmation(true);
        }
        break;
      case droneMsgsROS::actionData::ROTATE_YAW:
        if (abs(angles::shortest_angular_distance(current_pose.yaw,angles::normalize_angle(angles::from_degrees(current_action.arguments.at(0).value.at(0)))))<precision_null_rad_yaw && \
                abs(current_speed.dyaw) < precision_null_rotation_speed)
        {
          sendConfirmation(true);
        }
        break;
      case droneMsgsROS::actionData::FLIP_FRONT:
      case droneMsgsROS::actionData::FLIP:
      case droneMsgsROS::actionData::FLIP_RIGHT:
      case droneMsgsROS::actionData::FLIP_LEFT:
      case droneMsgsROS::actionData::FLIP_BACK:
        if ( abs(current_pose.pitch) < precision_null_attitude && abs(current_pose.roll) < precision_null_attitude && \
                abs(current_speed.dx) < precision_null_axis_speed && abs(current_speed.dy) < precision_null_axis_speed && \
                abs(current_speed.dz) < precision_null_axis_speed && abs(current_speed.dyaw) < precision_null_axis_speed && \
                abs(current_speed.dpitch) < precision_null_axis_speed && abs(current_speed.droll) < precision_null_axis_speed && \
                timer.getResult() )
        {
          sendConfirmation(true);
        }
        break;  
      default:
        ROS_FATAL("Checked unknown action");
    }
  }
}

void ActionMonitor::sendConfirmation(bool successful, bool timeout, bool interrupted)
{
  if (action_in_process)
  {
    droneMsgsROS::CompletedAction completed_action_msg;
    completed_action_msg.time = ros::Time::now();
    completed_action_msg.timeout = 20;//TODO Change to timeout variable.
    completed_action_msg.action.mpAction = current_action.mpAction;
    completed_action_msg.action.arguments=current_action.arguments;

    if (successful)
    {
      ROS_INFO("Action %s has been completed succesfully", action_name.c_str());
      completed_action_msg.final_state = droneMsgsROS::CompletedAction::SUCCESSFUL;
      completed_action_pub.publish(completed_action_msg);
    }
    else if (timeout)
    {
      ROS_WARN("Action %s has failed, it has taken too long", action_name.c_str());
      completed_action_msg.final_state = droneMsgsROS::CompletedAction::TIMEOUT_ACTIVATED;
      completed_action_pub.publish(completed_action_msg);
    }
    else if (interrupted)
    {
      ROS_WARN("Action %s has been interrupted", action_name.c_str());
      completed_action_msg.final_state = droneMsgsROS::CompletedAction::INTERRUPTED;
      completed_action_pub.publish(completed_action_msg);
    }
    else
    {
      //TODO, set better msg
      ROS_FATAL("Received unsuccessful mission, but has not been interrupted and timeout has not been activated");
    }

    action_in_process=false;
    current_speed_initialized=false;//the value stored in current_speed_initialized loses significance
    action_name="Error";
    waiting=false;
  }
  else
  {
    ROS_FATAL("Attempt to send a finished action when there is not action being monitored");
  }
}
