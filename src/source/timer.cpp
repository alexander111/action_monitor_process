#include "timer.h"

Timer::Timer()
{
  sleeping=false;
  secs=0;
  res=false;
  terminar=false;

  m_Thread = boost::thread(&Timer::threadFunc, this);
}

Timer::~Timer()
{
}

void Timer::time(int seconds)
{
  boost::mutex::scoped_lock lock(mut);
  secs=seconds;
  res=false;
  if (!sleeping)
  {
    cond.notify_one();
  }
  else
  {
    m_Thread.interrupt();
  }
}

void Timer::stop()
{
  boost::mutex::scoped_lock lock(mut);
  terminar=true;
  m_Thread.interrupt();
}

void Timer::join()
{
  stop();
  m_Thread.join();
}

void Timer::threadFunc()
{
  while (!terminar)
  {
    try
    {
      boost::mutex::scoped_lock lock(mut);
      while(secs<1)
      {
        cond.wait(lock);
      }

      int s=secs;
      sleeping=true;
      lock.unlock();

      //sleeps
      boost::this_thread::sleep_for(boost::chrono::seconds{s});
    
      //wakes up
      lock.lock();
      sleeping=false;
      secs=0;
      res=true;

    } catch (boost::thread_interrupted e)
    {
      continue;
    }
  }
}

bool Timer::getResult()
{
  boost::mutex::scoped_lock lock(mut);
  return res;
}